
import java.util.List;
import java.util.Scanner;

public class InfoMenu {
    public static Scanner scanner;

    static {
        scanner = new Scanner(System.in);
    }

    public void startMenuNotebook(){
        List<People> peopleList ;
        while (true){
            scanner = new Scanner(System.in);
            System.out.println("Открыть файл -ДА/НЕТ ");
            String control1 = scanner.nextLine();
            if (control1.equalsIgnoreCase("Да")){
                System.out.println("ВВедите имя файла");
                NoteBook noteBook = new NoteBook(scanner.nextLine());
                peopleList = noteBook.openFile();
                noteBook.show(peopleList);
                while (true) {
                    System.out.println("Желаете редактировать файл? ДА/НЕТ");
                    String control3 = scanner.nextLine();
                    if (control3.equalsIgnoreCase("Да"))
                    while (true) {
                        scanner = new Scanner(System.in);
                        System.out.println("Выберите действие \nДобавить\nИзменить\nУдалить");
                        String control2 = scanner.nextLine();
                        if (control2.equalsIgnoreCase("изменить")) {
                            System.out.println("Укажите индекс");
                            noteBook.upgrade(peopleList, scanner.nextInt());
                            noteBook.show(peopleList);
                            noteBook.saveFile(peopleList);
                            break;
                        } else if (control2.equalsIgnoreCase("удалить")) {
                            System.out.println("Укажите индекс");
                            noteBook.delete(peopleList, scanner.nextInt());
                            noteBook.saveFile(peopleList);
                            noteBook.show(peopleList);
                            break;
                        } else if (control2.equalsIgnoreCase("добавить")) {
                            noteBook.write(peopleList);
                            noteBook.saveFile(peopleList);
                            noteBook.show(peopleList);
                            break;
                        }

                    }else if (control3.equalsIgnoreCase("Нет")){
                        break;
                    }
                }
            }else if (NoteBook.scanner.nextLine().equalsIgnoreCase("Нет")){
                System.out.println("Создать новый файл?");
            }

        }

    }
}
