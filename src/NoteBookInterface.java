import java.util.List;

public interface NoteBookInterface {
     List<People> openFile() ;
     void saveFile(List<People> list);
     void clear(List<People>list);
     void write(List<People>list);
     void delete(List<People> list,int index);
     void upgrade(List<People> list,int index);
     void show(List<People> list);
}
