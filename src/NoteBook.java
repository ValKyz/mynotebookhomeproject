import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NoteBook implements NoteBookInterface {
    private String fileName;
    public static Scanner scanner;

    static {
        scanner = new Scanner(System.in);
    }

    public NoteBook(String fileName) {
        this.fileName = fileName;
    }

    public List<People> openFile2() {
        List<People> peopleList = new ArrayList<>();
        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                String lastName = parts[2];
                String phoneNumber = parts[3];
                peopleList.add(new People(id, name, lastName, phoneNumber));
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {

        }
        return peopleList;
    }

    @Override
    public List<People> openFile() {
        List<People> peopleList = new ArrayList<>();
        try {
            String[] splitted;
            scanner = new Scanner(new File(fileName));
            int id;
            String name;
            String lastName;
            String phoneNumber;
            while (scanner.hasNext()) {
                splitted = scanner.nextLine().split("\\|");
                id = Integer.parseInt(splitted[0]);
                name = splitted[1];
                lastName = splitted[2];
                phoneNumber = splitted[3];
                peopleList.add(new People(id, name, lastName, phoneNumber));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return peopleList;
    }

    @Override
    public void saveFile(List<People> list) {
        try (Writer writer = new FileWriter(fileName,false);BufferedWriter bufferedWriter = new BufferedWriter(writer)){
          for (int i = 0 ; i < list.size();i++){
              bufferedWriter.write(list.get(i).getId() + "|" + list.get(i).getFirstName()
                        + "|" + list.get(i).getLastName() +  "|" + list.get(i).getNumPhone());
              bufferedWriter.newLine();
              bufferedWriter.flush();
          }
        }catch (IOException e){
            throw  new IllegalArgumentException();
        }


    }



    @Override
    public void clear(List<People> list) {
        System.out.println("Очищен");
        list.clear();

    }

    @Override
    public void write(List<People> list) {
        scanner = new Scanner(System.in);
        int id = list.size() + 1;
        System.out.println("Укажите имя");
        String name = scanner.nextLine();
        System.out.println("Укажите фамилию");
        String lastName = scanner.nextLine();
        System.out.println("Укажите телефон");
        String phone = scanner.nextLine();
        People people = new People(id, name, lastName, phone);
        list.add(people);
    }


    @Override
    public void delete(List<People> list,int index) {
        list.remove(index - 1);
        for (int i = index - 1; i < list.size(); i++) {
            People people = list.get(i);
            people.setId(i + 1);
        }
    }

    @Override
    public void upgrade(List<People> list, int index) {
        scanner = new Scanner(System.in);
        People people = list.get(index-1);
        while (true){
            System.out.println("Укажите что хотите изменить /ИМЯ/ФАМИЛЯ/ТЕЛЕФОН");
            String control = scanner.nextLine();
            if (control.equalsIgnoreCase("имя")){
                System.out.println("Укажите новое имя");
                String name = scanner.nextLine();
                people.setFirstName(name);
                break;
            }else if (control.equalsIgnoreCase("фамилия")){
                System.out.println("Укажите новую фамилию");
                String lastName = scanner.nextLine();
                people.setLastName(lastName);
                break;
            } else if (control.equalsIgnoreCase("телефон")){
                System.out.println("Укажите новый телефон");
                String phone = scanner.nextLine();
                people.setNumPhone(phone);
                break;
            }
        }

    }

    @Override
    public void show(List<People> list) {
        for (Object people : list) {
            System.out.println(people);
        }
    }

}
